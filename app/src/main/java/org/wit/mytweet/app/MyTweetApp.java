package org.wit.mytweet.app;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import org.wit.mytweet.models.Token;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.wit.android.helpers.LogHelpers.info;


/**
 * MyTweetApp class to initialise and manage app
 */
public class MyTweetApp extends Application implements Callback<Token>
{
  public TweetServiceOpen tweetServiceOpen;
  public TweetService tweetService;

  public boolean tweetServiceAvailable = false;

  //public String service_url = "https://mytweet-web-02-ft21.herokuapp.com/"; // Android Emulator

  public User currentUser;

  public TweetList tweetList;
  private static final String FILENAME = "localtweets.json";
  public DbHelper dbHelper = null;

  protected static MyTweetApp app;

  //public List<Tweet> tweets   = new ArrayList<Tweet>();


  @Override
  public void onCreate()
  {
    super.onCreate();

    //initialise serializer and tweetlist upon app start
    //TweetListSerializer serializer = new TweetListSerializer(this, FILENAME);
    //tweetList = new TweetList(serializer);

    dbHelper = new DbHelper(getApplicationContext());
    tweetList = new TweetList(dbHelper);
    tweetList.tweets = (ArrayList<Tweet>) dbHelper.selectAllTweets();

    app = this;

    tweetServiceOpen = RetrofitServiceFactory.createService(TweetServiceOpen.class);
    info(this, tweetServiceOpen.toString());

  }

  /**
   * Returns the MyTweet global app object
   *
   * @return
   */
  public static MyTweetApp getApp()
  {
    return app;
  }

  /**
   * Adds a new user to the SQL Lite db
   *
   * @param user
   */
  public void newUser(User user)
  {
    dbHelper.addUser(user);
  }

  /**
   * Validates a user against the details stored in the db
   *
   * @param email
   * @param password
   * @return
   */
  public boolean validUser (String email, String password)
  {
    /*
    User dbUser = dbHelper.selectUser(email);
    if (dbUser.password.equals(password)) {
      return true;
    }
    return false;
    */

    //User user = new User ("", "", email, password);
    currentUser = new User ("", "", email, password);
    tweetServiceOpen.authenticateUser(currentUser);
    Call<Token> call = (Call<Token>) tweetServiceOpen.authenticateUser(currentUser);
    call.enqueue(this);
    return true;
  }

  @Override
  public void onResponse(Call<Token> call, Response<Token> response) {
    app.tweetServiceAvailable = true;
    Token auth = response.body();
    //currentUser = auth.user;
    tweetService =  RetrofitServiceFactory.createService(TweetService.class, auth.token);
    Log.v("MyTweet", "Authenticated " + currentUser.firstName + ' ' + currentUser.lastName);
  }

  @Override
  public void onFailure(Call<Token> call, Throwable t) {
    Toast toast = Toast.makeText(this, "Unable to authenticate with Tweet Service", Toast.LENGTH_LONG);
    toast.show();
    Log.v("MyTweet", "Failed to Authenticate user!");
    info(this, t.toString());
  }
}

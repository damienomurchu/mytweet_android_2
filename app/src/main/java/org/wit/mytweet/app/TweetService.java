package org.wit.mytweet.app;

import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface TweetService
{

  @GET("/api/users")
  Call<List<User>> findAllUsers();

  @DELETE("/api/users/{id}")
  Call<String> deleteUser(@Path("id") String id);

  @DELETE("/api/users")
  Call<String> deleteAllUsers();

  @POST("/api/tweets")
  Call<Tweet> sendTweet(@Body Tweet tweet);

  @DELETE("/api/tweets/{id}")
  Call<String> deleteTweet(@Path("id") String id);

  @DELETE("/api/tweets")
  Call<String> deleteAllTweets();
}

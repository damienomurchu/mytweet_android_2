package org.wit.mytweet.models;

import java.util.UUID;

/**
 * A model class that represents a registered app User
 */
public class User
{
  public UUID id;
  public String firstName;
  public String lastName;
  public String email;
  public String password;

  public User(String firstName, String lastName, String email, String password)
  {
    id = UUID.randomUUID();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  }
}

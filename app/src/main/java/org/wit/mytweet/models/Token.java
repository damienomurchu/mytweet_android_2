package org.wit.mytweet.models;

/**
 * Created by dmurphy on 21/12/16.
 */

public class Token
{
  public boolean success;
  public String token;
  public User user;

  public Token (boolean success, String token)
  {
    this.success = success;
    this.token = token;
  }
}
